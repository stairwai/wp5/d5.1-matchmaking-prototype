#!/bin/python3

from matchmaking import base
import pickle
import os

class StaticResourceDS:
    def __init__(self, labels, resdb_file):
        self.resdb_file = resdb_file
        self.labels = labels

    def clear_resources(self):
        if os.path.exists(self.resdb_file):
            os.remove(self.resdb_file)

    def put_resources(self, resources):
        old_res = self.get_resources()
        new_res = old_res + resources
        with open(self.resdb_file, 'wb') as fp:
            pickle.dump(new_res, fp)

    def get_resources(self):
        res = []
        if os.path.exists(self.resdb_file):
            with open(self.resdb_file, 'rb') as fp:
                res = pickle.load(fp)
        return res

