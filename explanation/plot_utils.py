import matplotlib.pyplot as plt
import pandas as pd
from IPython.display import display, HTML

def format_fn(tick_val, tick_pos):
    if int(tick_val)-1 in xs:
        return xticks[int(tick_val)-1]
    else:
        return 'BANANA'

def format_fn2(tick_val, tick_pos):
    if int(tick_val) in xs:
        return xticks[int(tick_val)]
    else:
        return 'BANANA'

def query_scores_boxplot(query_scores_df):
    """Plot util for boxplot and standard deviation of the query scores."""
    global xs 
    global xticks
    label_names = query_scores_df.columns
    xs = range(len(label_names))
    xticks = label_names

    diamond = dict(markerfacecolor='black', marker='D')
    fig, ax = plt.subplots(figsize=(20,6))
    boxplot = ax.boxplot(x = query_scores_df,
                           flierprops=diamond,
                            patch_artist=True,
                            vert=True,
                           notch=True
                        )
    plt.setp(boxplot["boxes"], facecolor='purple', alpha = 0.6)
    # ax.xaxis.set_major_formatter(format_fn)
    ax.set_title(label=f'Boxplot of query scores per label')
    ax.set_xticklabels(ax.get_xticklabels(), fontsize=14)
    ax.set_ylabel('Query score', fontsize = 14)
    ax.xaxis.set_major_formatter(format_fn)
    plt.xticks(rotation=90)
    plt.yticks(fontsize=14)
    plt.grid(True)
    
    
    fig, ax = plt.subplots(figsize=(20,6))
    query_scores_df.std().plot(kind='bar', 
                               grid=True, 
                              alpha = 0.6,
                              color='purple',
                            ylabel ='std')
    # ax.xaxis.set_major_formatter(format_fn)
    ax.set_title(label=f'Standard deviation of query scores per label');
    ax.set_xticklabels(ax.get_xticklabels(), fontsize=14)
    ax.set_ylabel('Query score STD', fontsize = 14)
    ax.xaxis.set_major_formatter(format_fn2)
    plt.yticks(fontsize=14);


def query_scores_bar(query, query_scores, labels):
    """Plot util for boxplot and standard deviation of the query scores."""
    global xs 
    global xticks
    label_names = [l.name for l in labels]
    xs = range(len(label_names))
    xticks = label_names

    scores_as_df = pd.Series(data=query_scores)
    scores_as_df.rename(index={l.identifier:l.name for i, l in enumerate(labels)},
            inplace=True)

    # Display the query
    display(HTML(f'<strong>Query:</strong> {query}'))

    # fig, ax = plt.subplots(figsize=(20,3))
    scores_as_df.plot(kind='bar', 
                       grid=True, 
                      alpha = 0.6,
                      color='purple',
                    ylabel ='score',
                    figsize=(20, 4))
    # ax.xaxis.set_major_formatter(format_fn)
    # ax.set_title(label=f'Query scores');
    # ax.set_xticklabels(ax.get_xticklabels(), fontsize=14)
    # ax.set_ylabel('Query score', fontsize = 14)
    # ax.xaxis.set_major_formatter(format_fn2)
    # plt.yticks(fontsize=14);

# def query_scores_boxplot(query_scores_df, label_names):
#     """Plot util for boxplot and standard deviation of the query scores."""
#     global xs 
#     global xticks
#     xs = range(len(label_names))
#     xticks = label_names

#     diamond = dict(markerfacecolor='black', marker='D')
#     fig, ax = plt.subplots(figsize=(20,6))
#     boxplot = ax.boxplot(x = query_scores_df,
#                            flierprops=diamond,
#                             patch_artist=True,
#                             vert=True,
#                            notch=True
#                         )
#     plt.setp(boxplot["boxes"], facecolor='purple', alpha = 0.6)
#     ax.xaxis.set_major_formatter(format_fn)
#     ax.set_title(label=f'Boxplot of query scores per label')
#     ax.set_xticklabels(ax.get_xticklabels(), fontsize=14)
#     ax.set_ylabel('Query score', fontsize = 14)
#     ax.xaxis.set_major_formatter(format_fn)
#     plt.xticks(rotation=90)
#     plt.yticks(fontsize=14)
#     plt.grid(True)
    
    
#     fig, ax = plt.subplots(figsize=(20,6))
#     query_scores_df.std().plot(kind='bar', 
#                                grid=True, 
#                               alpha = 0.6,
#                               color='purple',
#                             ylabel ='std')
#     ax.xaxis.set_major_formatter(format_fn)
#     ax.set_title(label=f'Standard deviation of query scores per label');
#     ax.set_xticklabels(ax.get_xticklabels(), fontsize=14)
#     ax.set_ylabel('Query score STD', fontsize = 14)
#     ax.xaxis.set_major_formatter(format_fn)
#     plt.yticks(fontsize=14);
    
    
def plot_relative_scores(query_relative_scores, asset_relative_scores, query_name,
        asset_name, label_names, title=None):
    """Compares query and asset relative scores."""
    
    global xs 
    global xticks
    xs = range(len(label_names))
    xticks = label_names
  
    fig, ax = plt.subplots()
    query_relative_scores.plot(kind='bar', 
                               grid=True, 
                               figsize=(20,6), 
                              alpha = 0.5,
                              label = f'Query: {query_name}',
                              color='purple')
    asset_relative_scores.plot(kind='bar', 
                                      grid=True, 
                                      figsize=(20,6), 
                                     color='orange',
                                     alpha = 0.4,
                                     label = f'Asset: {asset_name}')
    ax.set_xticklabels(ax.get_xticklabels(), fontsize=14)
    ax.set_ylabel('Relative score', fontsize = 14)
    ax.xaxis.set_major_formatter(format_fn2)
    plt.title(title)
    plt.yticks(fontsize=14)
    plt.ylim((-1, 1))
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, 1.10),
          ncol=3, fancybox=True, shadow=True, fontsize=14);
    
