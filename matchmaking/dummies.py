#!/usr/bin/python

from matchmaking import base
import numpy as np

class DummyTextLabeler(base.TextLabeler):

    def __init__(self):
        super(DummyTextLabeler, self).__init__()
        self.rng = None

    def configure(self, seed):
        self.rng = np.random.default_rng(seed)
        super(DummyTextLabeler, self).configure()

    def score(self, text_list):
        # Assign a random score to each label
        scores = {l.identifier: self.rng.random() for l in self.labels}
        return scores
        
        
