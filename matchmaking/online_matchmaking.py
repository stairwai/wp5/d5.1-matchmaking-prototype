import numpy as np
from matchmaking import base
from sklearn.feature_extraction import DictVectorizer


class OnlineMatchmaking(base.MatchmakingAlgorithm):

        def __init__(self, metrics):
            """
            Parameters:
            - metrics: matchmaking distance function
            """
            self.metrics = metrics
            self.labels = []
            self.relations = []
            self.resources = []

        def loadLabels(self, labels, relations):
            """
            Parameters:
            - labels: a list of Label objects
            - relations: a dictionary with:
              - keys: type of relation
              - values: list of tuples (label1, label2)
            """
            self.labels = labels
            self.relations = relations

        def loadResources(self, resources):
            """
            Parameters:
            - resources: a list of resources
            """
            self.resources = resources
            self.resources_scores = [r.scores for r in resources]

        def configure(self):
            """
            Perform any internal configuration here
            """
            raise NotImplementedError('This methods needs to be implemented')

        def matchOne(self, query_scores, query_metadata=None, args={}):
            """
            Parameters:
            - query_scores: a dictionary with
              - keys: label idx
              - values: score for each label
            - metadata: any other symbolic information
              - TODO: define format
            - args: arguments of the distance function

            Returns:
            - list of ascending ordered (resource_identifier, matching_score)
            - explanations
                -TODO: define format
            """
            # Check resources
            assert len(self.resources) > 0, "No resources found."

            # Transform resource and query into sparse matrices
            vectorizer = DictVectorizer(sparse=True)
            resource_matrix = vectorizer.fit_transform(self.resources_scores)
            query_v = vectorizer.transform(query_scores)

            # Compute matching
            matching_scores = self.metrics(query_v, resource_matrix, **args)[0]

            # Sort resource indexes
            resource_idxs = np.argsort(-matching_scores)

            # Return list of (resource_identifier, matching_score)
            ordered_matching = [(idx, matching_scores[idx]) for idx in resource_idxs]
            return ordered_matching
