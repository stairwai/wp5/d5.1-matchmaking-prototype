#!/bin/python3

from flask import Flask
from flask import request
from markupsafe import escape
from flask import render_template
from util import local
import resdb
import json
import os
# TODO replace this with a unified logger, or hide it in the labeler implementation
from labeler.unibo.utility.log_utils import Logger


# ==============================================================================
# Service setup
# ==============================================================================

# Build the Flask app
app = Flask(__name__)

# Read configuration parameters
params = None
with open('conf.json') as fp:
    params = json.load(fp)

# Load labels
label_fname = os.path.normpath(params['label_fname'])
labels = local.load_labels(label_fname)

# Load the labeler
log_dir = os.path.normpath(params['log_dir'])
log_path = os.path.join(log_dir, Logger.logging_dir)
labeler = local.build_unibo_labeler(label_fname, log_path)

# Load the resources
resdb_fname = os.path.normpath(params['resource_db'])
rds = resdb.StaticResourceDS(labels, resdb_fname)

# ==============================================================================
# Utility functions
# ==============================================================================

# Run the matchmaking algorithm
def run_matchmaking(query):
    # Compute query scores
    q_scores = labeler.score([query])
    q_scores = q_scores[0]
    # Load the resources
    res_list = rds.get_resources()
    # Build the matchmaking algorithm
    online_alg = local.build_online_matchmaking(res_list)
    # Run the matchmaking algorithm for the query
    mm_res = online_alg.matchOne(query_scores=q_scores)
    # Convert the resource index to a dictionary
    # Prepare result data structure
    res = {'resources': []}
    for idx, score in mm_res:
        rdesc = {'id': res_list[idx].identifier,
                 'name': res_list[idx].name,
                 'score': score}
        res['resources'].append(rdesc)
    # Return results
    return res

# ==============================================================================
# Routes
# ==============================================================================

# Define the main route
@app.route('/query', methods=['GET', 'POST'])
def query():
    if request.method == 'POST':
        return run_matchmaking(request.form['query'])
    else:
        # Show a simple query form
        return render_template('simple_query_form.html')

