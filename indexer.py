#!/bin/python3

# TODO Replace this dependency with a centralized logger
from labeler.unibo.utility.log_utils import Logger
from util import local
import resdb
import json
import os

if __name__ == "__main__":
    # Read configuration parameters
    params = None
    with open('conf.json') as fp:
        params = json.load(fp)

    # Load labels
    label_fname = os.path.normpath(params['label_fname'])
    labels = local.load_labels(label_fname)

    # Load resources
    # TODO load these from the graph DB server instead
    resource_fname = os.path.normpath(params['resource_fname'])
    resources = local.load_resources(resource_fname)

    # Load the labeler
    log_dir = os.path.normpath(params['log_dir'])
    log_path = os.path.join(log_dir, Logger.logging_dir)
    labeler = local.build_unibo_labeler(label_fname, log_path)

    # Label the resources
    resource_descriptions = [d for d in resources['Description']]
    resource_scores = labeler.score(resource_descriptions)
    resource_list = local.build_resource_index(resources, resource_scores)

    # Build a static data source
    resdb_fname = os.path.normpath(params['resource_db'])
    rds = resdb.StaticResourceDS(labels, resdb_fname)

    # Clear the resource list
    rds.clear_resources()

    # Store resources
    rds.put_resources(resource_list)

