#!/usr/bin/python

class Label():
    def __init__(self, identifier, name, description):
        self.identifier = identifier
        self.name = name
        self.description = description

    def __repr__(self):
        return 'label({}, {})'.format(self.identifier, self.name)


class TextLabeler:
    def __init__(self):
        self.labels = []
        self.relations = []

    def loadLabels(self, labels, relations):
        """
        Parameters:
        - labels: a list of Label objects
        - relations: a dictionary with:
          - keys: type of relation
          - values: list of tuples (label1, label2)
        """
        self.labels = labels
        self.relations = relations

    def configure(self):
        """
        Perform any internal configuration here
        """
        pass

    def score(self, text_list):
        """
        Assigns score

        Parameters:
        - text_list: a list of strings

        Returns:
        - a dictionary with:
          - keys: label identifier
          - values: scores
        """
        raise NotImplementedError('This methods needs to be implemented')


# class Resource():
#     def __init__(self, identifier, name, scores):
#         self.identifier = identifier
#         self.name = name
#         self.scores = scores

#     def __repr__(self):
#         return '{}({})'.format(self.identifier, self.name)


# class MatchmakingAlgorithm:

#     def __init_(self):
#         self.labels = []
#         self.relations = []
#         self.resources = []

#     def loadLabels(self, labels, relations):
#         """
#         Parameters:
#         - labels: a list of Label objects
#         - relations: a dictionary with:
#           - keys: type of relation
#           - values: list of tuples (label1, label2)
#         """
#         self.labels = labels
#         self.relations = relations

#     def loadResources(self, resources):
#         """
#         Parameters:
#         - resources: a list of resources
#         """
#         self.resources = resources

#     def configure(self):
#         """
#         Perform any internal configuration here
#         """
#         raise NotImplementedError('This methods needs to be implemented')

#     def matchOne(self, query_scores, query_metadata):
#         """
#         Parameters:
#         - query_scores: a dictionary with
#           - keys: label idx
#           - values: score for each label
#         - metadata: any other symbolic information 
#           - TODO: define format

#         Returns:
#         - list of resource identifiers
#         - explanations (in some format)
#         """
#         raise NotImplementedError('This methods needs to be implemented')

#     def matchMany(self, query_scores, query_metadata, resource_capacity):
#         """
#         Parameters:
#         - query_scores: a dictionary with
#           - keys: label idx
#           - values: score for each label
#         - metadata: any other symbolic information 
#           - TODO: define format
#         - resource_capacity: maximum number of resources to return

#         Returns:
#         - list of list
#           - Each list contains resource idenfiers for a query
#         - explanations (in some format)
#         """
#         raise NotImplementedError('This methods needs to be implemented')
