import abc
from typing import List, Dict, AnyStr, Union
# from dev_startkit.utility.log_utils import Logger
from labeler.unibo.utility.log_utils import Logger


class Label(object):
    """
    Stores a ontology label and its textual information
    """

    def __init__(self, identifier: int, name: AnyStr, description: AnyStr,
                 synonyms: Union[List[str], None] = None):
        """
        Instantiates a Label object.

        Args:
            identifier (int): an integer that uniquely identifies a ontology label
            name (str): the name of the label
            description (str): a long textual description (a few sentences) of the label.
            synonyms (List[str], Optional): a list of names, each being a synonym of the Label's name.
        """

        self.identifier = identifier
        self.name = name
        self.description = description
        self.synonyms = synonyms

    def __repr__(self):
        return f'label({self.identifier}, {self.name})'


class TextLabeler(metaclass=abc.ABCMeta):
    """
    A labeler for matching input textual documents to ontology labels.
    """

    def __init__(self):
        """
        Instantiates a TextLabeler object.
        Wrappers for Label and possible label relations are initialized.
        """

        self.labels = []
        self.relations = []

    def load_labels(self, labels: List[Label], relations: Dict[str, List]):
        """
        Stores input labels and relations as instance attributes.

        Args:
            labels (List[Label]): a list of Label objects
            relations (Dict[str, List]): a dictionary where:
                - keys: type of relation
                - values: list of tuples (label1, label2)
        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Attempting to store new information:\n'
                                         f'Labels: {labels}\n'
                                         f'Relations: {relations}')

        # Check for unique labels
        identifiers = set([label.identifier for label in labels])
        assert len(identifiers) == len(labels)

        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Labels and relations successfully stored!')

        self.labels = labels
        self.relations = relations

    @abc.abstractmethod
    def configure(self):
        """
        Configure a TextLabeler instance.
        Usually, this method involves loading labels and their relations.
        Other configuration actions should be executed here.
        """
        pass

    @abc.abstractmethod
    def score(self, text_list: List[str]) -> List[Dict[str, float]]:
        """
        Computes ontology matching for each input text in the given list.
        Ontology matching for an input text involves computing similarity scores for each ontology label.

        Args:
            text_list (List[str]): a list of textual descriptions, each representing a resource (e.g. an Asset).

        Returns:
            a list of dictionary, one for each input resource, where:
                key: label's identifier
                value: the similarity obtained for that label.
        """
        pass
