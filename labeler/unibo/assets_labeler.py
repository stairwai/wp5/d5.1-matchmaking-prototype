import os
from typing import List, Dict, AnyStr

import numpy as np
import pandas as pd # TODO remove pandas dependency, if possible
import spacy
# from dev_startkit.matchmaking.labeler import TextLabeler, Label
from labeler.unibo.labeler import TextLabeler, Label
from sentence_transformers import SentenceTransformer
from tqdm import tqdm

# from dev_startkit.utility.json_utils import load_json
# from dev_startkit.utility.log_utils import Logger
# from dev_startkit.utility.metric_utils import get_metric
# from dev_startkit.utility.pickle_utils import save_pickle, load_pickle

from labeler.unibo.utility.json_utils import load_json
from labeler.unibo.utility.log_utils import Logger
from labeler.unibo.utility.metric_utils import get_metric
from labeler.unibo.utility.pickle_utils import save_pickle, load_pickle


class SBertLabel(Label):
    """
    An extension of the standard Label to support S-BERT embeddings
    """

    def __init__(self, description_sentences: List[str], **kwargs):
        """
        Instantiates a SBertLabel object.

        Args:
            description_sentences (List[str]): the text description of a Label split into sentences.
        """

        super(SBertLabel, self).__init__(**kwargs)
        self.description_sentences = description_sentences

        # Embedding
        self.name_embedding = None
        self.description_sentences_embedding = None
        self.synonyms_embedding = None

    def embed(self, sbert_model: SentenceTransformer):
        """
        Encodes a Label's textual data with a S-BERT model.
        Embeddings are stored as instance attributes to avoid further re-computations.

        Args:
            sbert_model (SentenceTransformer): an instance of a S-BERT model.
        """

        self.name_embedding = sbert_model.encode([self.name])
        self.description_sentences_embedding = sbert_model.encode(self.description_sentences)
        if len(self.synonyms):
            self.synonyms_embedding = sbert_model.encode(self.synonyms)


class SBertLabeler(TextLabeler):
    """
    An extension of the standard TextLabeler to support S-BERT encoding.
    """

    def __init__(self, labels_filepath: AnyStr, spacy_model_name: AnyStr = 'en_core_web_sm',
                 strategy: AnyStr = 'descr2text', sbert_model_name: AnyStr = 'all-mpnet-base-v2',
                 similarity_metric: AnyStr = 'cosine_similarity', save_path: AnyStr = None):
        """
        Instantiates a SBertLabeler object.

        Args:
            labels_filepath (str): the path where labels (in textual format) are stored.
            spacy_model_name (str): the name of the SpaCy tokenizer model to use for sentence splitting.
            strategy (str): the name of strategy to use for performing unsupervised ontology matching.
            sbert_model_name (str): the name of a S-BERT model to instantiate for embedding textual data.
            similarity_metric (str): the name of the similarity metric to adopt for comparing embedding vectors.
            save_path (str): the path where to save or load SBertLabeler data (e.g., configuration files).
        """

        super(SBertLabeler, self).__init__()
        self.labels_filepath = labels_filepath
        self.spacy_model_name = spacy_model_name
        self.spacy_model = spacy.load(spacy_model_name)
        self.spacy_model.add_pipe('sentencizer')
        self.strategy = strategy
        self.sbert_model_name = sbert_model_name
        self.sbert_model = SentenceTransformer(sbert_model_name)
        self.similarity_metric, self.is_metric_embedding_based = get_metric(similarity_metric)
        self.save_path = save_path if save_path is not None else os.getcwd()

        if not os.path.isdir(self.save_path):
            os.makedirs(self.save_path)

        self.score_strategy_map = {
            'name2text': self._score_name2text,
            'descr2text': self._score_descr2text,
        }

        assert strategy in self.score_strategy_map.keys(), \
            f"Strategy not supported! Got {strategy}, but currently supporting {list(self.score_strategy_map.keys())}"

    def _sentence_split_and_filter(self, text: AnyStr) -> List[str]:
        """
        Splits a text into sentences and filters out empty sentences.

        Args:
            text (str): the text to be split into sentences.

        Returns:
             a list of sentences extracted by using the SpaCy tokenizer.
        """

        sentences = [sentence.text for sentence in self.spacy_model(text).sents if len(sentence.text.strip())]
        return sentences

    def _load_labels_json(self) -> List[Label]:
        """
        Loads label textual data from a .json file.
        For each loaded label, a SBertLabel instance is created and stored in an list.

        Returns:
            a list of SBertLabel objects, one for each loaded label.
        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Loading labels from JSON file...')

        labels_data = load_json(self.labels_filepath)['labels']

        label_list = []
        for label_item in labels_data:
            # Sentence split
            description_sentences = self._sentence_split_and_filter(text=label_item['description'])

            # Synonyms
            synonyms = label_item['related terms']
            if type(synonyms) != float:
                synonyms = synonyms.replace('...', '')
                synonyms = synonyms.split(',')
                synonyms = [item.strip() for item in synonyms if len(item.strip())]
            else:
                synonyms = []

            label = SBertLabel(identifier=int(label_item['identifier']),
                               name=label_item['name'],
                               description=label_item['description'],
                               description_sentences=description_sentences,
                               synonyms=synonyms)
            label.embed(self.sbert_model)
            label_list.append(label)

        return label_list

    def _load_labels_csv(self) -> List[Label]:
        """
        Loads label textual data from a .csv file.
        For each loaded label, a SBertLabel instance is created and stored in an list.

        Returns:
            a list of SBertLabel objects, one for each loaded label.
        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Loading labels from CSV file...')

        # TODO pass the pre-parsed labels instead
        labels_df = pd.read_csv(self.labels_filepath)
        label_names = labels_df.term.values
        label_descriptions = labels_df.definition.values
        label_synonyms = labels_df['informal synonyms'].values

        label_list = []
        for identifier, (name, description, synonyms) in enumerate(zip(label_names, label_descriptions, label_synonyms)):
            # Sentence split
            description_sentences = self._sentence_split_and_filter(text=description)

            # Synonyms
            if type(synonyms) != float:
                synonyms = synonyms.replace('...', '')
                synonyms = synonyms.split(',')
                synonyms = [item.strip() for item in synonyms if len(item.strip())]
            else:
                synonyms = []

            label = SBertLabel(identifier=identifier, name=name,
                               description=description, description_sentences=description_sentences,
                               synonyms=synonyms)
            label.embed(self.sbert_model)
            label_list.append(label)

        return label_list

    # TODO: upgrade when relations are defined
    def _load_relations_csv(self) -> Dict[str, List]:
        """
        Loads label relations from a .csv file.

        Returns:
            a dictionary where:
                key: relation type
                value: a tuple of labels' identifiers. Each tuple denotes that there is a relation
                between the reported labels.
        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Loading label relations from CSV file...')

        return {}

    # TODO: upgrade when relations are defined
    def _load_relations_json(self) -> Dict[str, List]:
        """
        Loads label relations from a .csv file.

        Returns:
            a dictionary where:
                key: relation type
                value: a tuple of labels' identifiers. Each tuple denotes that there is a relation
                between the reported labels.
        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Loading label relations from CSV file...')

        return {}

    def configure(self, force_reload: bool = False):
        """
        Configures a SBertLabeler instance.
        The configuration process involves loading labels and their relations from serialized data.
        Currently the method only supports label loading from .csv files.

        Args:
            force_reload (bool): forces label data reloading if True.

        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Configuring...')
        load_path = os.path.join(self.save_path, 'labeler_data.pickle')

        if self.labels_filepath.endswith('.csv'):
            labels_function = self._load_labels_csv
            relations_function = self._load_relations_csv
        elif self.labels_filepath.endswith('json'):
            labels_function = self._load_labels_json
            relations_function = self._load_relations_json
        else:
            raise NotImplementedError(f'Labels filepath is not in .csv or .json format - Got {self.labels_filepath}.'
                                      f'This method only supports .csv or .json format at the moment.')

        # Load if not already done
        if not os.path.isfile(load_path) or force_reload:
            # Load
            labels = labels_function()
            relations = relations_function()

            # Save
            loaded_data = {
                'labels': labels,
                'relations': relations
            }
            save_pickle(load_path, loaded_data)
        else:
            # Load
            Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Loading serialized labels'
                                             f' and label relations....')

            loaded_data = load_pickle(load_path)
            labels, relations = loaded_data['labels'], loaded_data['relations']

        # Set
        self.load_labels(labels=labels, relations=relations)

    def _score_name2text(self, text_sentences: List[str]) -> Dict[str, float]:
        """
        Computes the similarity between an input sentence list, representing a single resource (e.g., an Asset),
        and the stored list of labels.

        The similarity is computed according to the 'name2text' strategy. In this strategy, the method
        computes the similarity between: a input list of sentences and each label's name and synonyms.

        Args:
            text_sentences (List[str]): a list of input text sentences representing a single resource (e.g., an Asset).

        Returns:
            a dictionary where:
                key: label's identifier
                value: the maximum similarity obtained for that label.
        """

        # Sanity checks
        assert self.labels is not None, "No label data found. " \
                                        "Perhaps, you have to load label data by running the configure() method."

        # M Sentences
        if self.is_metric_embedding_based:
            text_sentences_embedding = self.sbert_model.encode(text_sentences)
        else:
            text_sentences_embedding = text_sentences

        # N Labels
        scores = {}
        for label in self.labels:
            if not hasattr(label, 'name'):
                raise RuntimeError(f"Could not find attribute 'name' for label. Label object attributes: {vars(label)}")

            if self.is_metric_embedding_based:
                label_embedding = label.name_embedding
            else:
                label_embedding = label.name

            # Consider synonyms (if available)
            if len(label.synonyms):
                if self.is_metric_embedding_based:
                    label_embedding = np.vstack((label_embedding, label.synonyms_embedding))
                else:
                    label_embedding = [label_embedding] + label.synonyms

            # Similarity
            label_similarities = self.similarity_metric(label_embedding, text_sentences_embedding)

            # Take max score
            label_similarity = np.max(label_similarities)

            # Memorize
            scores[label.identifier] = label_similarity

        return scores

    def _score_descr2text(self, text_sentences: List[str]) -> Dict[str, float]:
        """
        Computes the similarity between an input sentence list, representing a single resource (e.g., an Asset),
        and the stored list of labels.

        The similarity is computed according to the 'descr2text' strategy. In this strategy, the method
        computes the similarity between: a input list of sentences and each label's textual description.

        Args:
            text_sentences (List[str]): a list of input text sentences representing a single resource (e.g., an Asset).

        Returns:
            a dictionary where:
                key: label's identifier
                value: the maximum similarity obtained for that label.
        """

        # M Sentences
        if self.is_metric_embedding_based:
            text_sentences_embedding = self.sbert_model.encode(text_sentences)
        else:
            text_sentences_embedding = text_sentences

        # N Labels
        scores = {}
        for label in self.labels:
            # Skip malformed labels
            if not label.description_sentences:
                continue

            if self.is_metric_embedding_based:
                label_embedding = label.description_sentences_embedding
            else:
                label_embedding = label.description_sentences

            # Similarity
            label_similarities = self.similarity_metric(label_embedding, text_sentences_embedding)

            # Take max score
            label_similarity = np.max(label_similarities)

            # Memorize
            scores[label.identifier] = label_similarity

        return scores

    def score(self, text_list: List[str]) -> List[Dict[str, float]]:
        """
        Computes the unsupervised ontology matching for a input list of resources (e.g., Assets).
        Each input resource is parsed and scored according to the SBertLabeler's strategy.

        Args:
            text_list (List[str]): a list of textual descriptions, each representing a resource (e.g. an Asset).

        Returns:
            a list of dictionary, one for each input resource, where:
                key: label's identifier
                value: the maximum similarity obtained for that label.
        """
        Logger.get_logger(__name__).info(f'[{self.__class__.__name__}] Starting ontology matching:\n'
                                         f'Total documents: {len(text_list)}\n'
                                         f'Total ontology labels: {len(self.labels)}\n'
                                         f'Strategy: {self.strategy}\n'
                                         f'S-BERT model: {self.sbert_model}')

        scores = []
        for text in tqdm(text_list):
            # Get sentences
            text_sentences = self._sentence_split_and_filter(text=text)

            text_scores = self.score_strategy_map[self.strategy](text_sentences=text_sentences)
            scores.append(text_scores)

        return scores
