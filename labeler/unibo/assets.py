import os
import pandas as pd
# from dev_startkit.utility.pickle_utils import save_pickle, load_pickle
from matchmaking.labeler.unibo.utility.pickle_utils import save_pickle, load_pickle
import re
from typing import AnyStr, List, Dict, Any


class Asset(object):
    """
    Stores an AI Asset for ontology matching
    """

    def __init__(self, identifier: int, name: AnyStr, description: AnyStr,
                 characteristics: AnyStr = None, tags: List[AnyStr] = None):
        """
        Instantiates an Asset object.

        Args:
            identifier (int): an integer that uniquely identifies an Asset.
            name (str): name of the Asset. Usually, the Asset's title is used.
            description (str): a brief description of the Asset.
            characteristics (str): a detailed description of the Asset about its main characteristics.
            tags (List[str]): a list of tags that categorize the Asset.
        """

        self.identifier = identifier
        self.name = name
        self.description = description
        self.characteristics = characteristics
        self.tags = tags


class AssetsManager(object):
    """
    A wrapper of multiple Asset objects.
    """

    def __init__(self, columns_mapping: Dict[AnyStr, AnyStr], save_path: AnyStr = None):
        """
        Instantiates an AssetsManager object.

        Args:
            columns_mapping (Dict[str, str]): a dictionary where:
                key: the attribute's name of the Asset class
                value: the key's name of a serialized data structure under which the Asset's attribute data is stored.
                For instance, if the serialized data structure is a pandas.DataFrame, the value is the column name that
                contains the data that corresponds to the Asset's attribute used as key in the dictionary.
                E.g.: key = description and value = 'my_description_column' in a pandas.DataFrame object.
            save_path (str): the path where to save the AssetsManager's instance information. For instance,
            loaded Asset objects are serialized to avoid further loading operations.
        """

        self.columns_mapping = columns_mapping
        self.save_path = save_path if save_path is not None else os.getcwd()

        self.assets_list = []

        if save_path is not None and os.path.isdir(self.save_path):
            os.makedirs(self.save_path)

    def _load_assets_from_csv(self, assets_path: AnyStr) -> List[Asset]:
        """
        Reads a .csv file containing information about assets and builds a list of Asset objects with read information.

        Args:
            assets_path (str): the path where the .csv file containing assets information is stored.

        Returns:
            a list of Asset objects, each one being built from a row of the .csv file.
        """

        assets_df = pd.read_csv(assets_path)
        assets_list = []

        for row_idx, row in assets_df.iterrows():
            name_key = self.columns_mapping['name']
            description_key = self.columns_mapping['description']
            characteristics_key = self.columns_mapping['characteristics'] if 'characteristics' in self.columns_mapping else None
            tags_key = self.columns_mapping['tags'] if 'tags' in self.columns_mapping else None

            # identifier
            identifier = row_idx

            # name
            assert name_key in row, f'Column {name_key} could not be found!'
            name = row[name_key]

            # description
            assert description_key in row, f'Column {description_key} could not be found!'
            description = row[description_key]

            # mask hyperlinks
            description = re.sub(r'https?:\/\/.*[\r\n]*', '[LINK]', description, flags=re.MULTILINE)

            # characteristics
            if characteristics_key is not None and characteristics_key in row:
                characteristics = row[characteristics_key]

                # mask hyperlinks
                characteristics = re.sub(r'https?:\/\/.*[\r\n]*', '[LINK]', characteristics, flags=re.MULTILINE)
            else:
                characteristics = None

            # tags
            if tags_key is not None and tags_key in row:
                tags = row[tags_key].split(',') if type(row[tags_key]) != float else []
                tags = [item.strip() for item in tags if len(item.strip())]
            else:
                tags = None

            # Build Asset object
            asset = Asset(identifier=identifier, name=name, description=description,
                          characteristics=characteristics, tags=tags)
            assets_list.append(asset)

        return assets_list

    def to_text_list(self, key: AnyStr) -> List[Any]:
        """
        Gets the value of the attribute given as input (key) from each stored Asset.

        Args:
            key (str): the name of an attribute of the Asset class.

        Returns:
            the value of the given attribute from each stored Asset object.
        """

        assert key in self.columns_mapping.keys()
        return [getattr(asset, key) for asset in self.assets_list]

    def load_assets(self, assets_path: AnyStr, force_reload: bool = False):
        """
        Loads assets information from a serialized data structure.

        Args:
            assets_path (str): the path where the serialized data structure containing assets information is stored.
            force_reload (bool): forces runtime assets loading if True.
        """

        assert os.path.isfile(assets_path)

        # Loading from .csv
        if assets_path.endswith('.csv'):
            load_path = os.path.join(self.save_path, 'asset_manager_data.pickle')

            if not os.path.isfile(load_path) or force_reload:
                self.assets_list = self._load_assets_from_csv(assets_path=assets_path)

                # Save
                save_pickle(load_path, self.assets_list)
            else:
                # Load
                self.assets_list = load_pickle(load_path)
        else:
            raise NotImplementedError(f'Assets filepath is not in .csv format - Got {assets_path}.'
                                      f'This method only supports .csv format at the moment.')
