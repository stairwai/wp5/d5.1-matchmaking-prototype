
import os

# DEFAULT DIRECTORIES
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))

LOCAL_STANDARD_DATASETS_DIR = os.path.join(PROJECT_DIR, 'data')