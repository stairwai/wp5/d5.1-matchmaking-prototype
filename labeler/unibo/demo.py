"""

Demo examples for performing ontology matching for assets.

"""

from dev_startkit.matchmaking.assets_labeler import SBertLabeler
from dev_startkit.matchmaking.assets import AssetsManager
import os
from dev_startkit import const_define as cd
from dev_startkit.utility.log_utils import Logger


if __name__ == '__main__':

    Logger.set_log_path(os.path.join(cd.PROJECT_DIR, Logger.logging_dir))

    # Step 0: Configuration
    assets_path = os.path.join(cd.LOCAL_STANDARD_DATASETS_DIR, 'resources', 'ai-catalog-v2.csv')
    assets_managers_info = {
        'columns_mapping': {
            'name': 'Name',
            'description': 'Description',
        },
        'save_path': None
    }

    labeler_info = {
        'labels_filepath': os.path.join(cd.LOCAL_STANDARD_DATASETS_DIR, 'labels', 'labels.json'),
        'strategy': 'descr2text',
        'similarity_metric': 'cosine_similarity',
        'save_path': None
    }

    # Step 1: Loading Assets
    assets_manager = AssetsManager(**assets_managers_info)
    assets_manager.load_assets(assets_path=assets_path)

    # Step 2: Configuring TextLabeler
    labeler = SBertLabeler(**labeler_info)
    labeler.configure()

    # Step 3: Perform ontology matching
    # For each asset -> a dictionary where
    #   key -> label identifier
    #   value -> matching score
    # We can build Resource objects with this data
    scores = labeler.score(text_list=assets_manager.to_text_list(key='description'))
    for doc_scores in scores:
        print(doc_scores)
        print()
        print()
